/*
Ejercicio 1
Hacer uso de las variables de entrada y salida de datos de C++ para desarrollar el clásico juego Tic-Tac-Toe.
El juego debe contar con los siguientes modos de juego:
Jugador contra Jugador
Jugador contra máquina (opcional).
Los jugadores deben poder ingresar sus nombres y al final de la partida se debe informar el ganador. 
El tablero debe imprimirse después de cada jugada.
*/

#include <stdint.h>
#include <string>
#include <iostream>
#include <time.h>


using namespace std;

int check_winner (char *tablero)
{
    if (tablero[0] == tablero[1] && tablero[1] == tablero[2] && tablero[2] != ' ')
        return 1;

    if (tablero[3] == tablero[4] && tablero[4] == tablero[5] && tablero[5] != ' ')
        return 1;
    
    if (tablero[6] == tablero[7] && tablero[7] == tablero[8] && tablero[8] != ' ')
        return 1;
    
    if (tablero[0] == tablero[3] && tablero[3] == tablero[6] && tablero[6] != ' ')
        return 1;
    
    if (tablero[1] == tablero[4] && tablero[4] == tablero[7] && tablero[7] != ' ')
        return 1;
                    
    if (tablero[2] == tablero[5] && tablero[5] == tablero[8] && tablero[8] != ' ')
        return 1;
      
    if (tablero[0] == tablero[4] && tablero[4] == tablero[8] && tablero[8] != ' ')
        return 1;
    
    if (tablero[6] == tablero[4] && tablero[4] == tablero[2] && tablero[2] != ' ')
        return 1;
     
    return 0;
}

int main()
{
    int flagGanador;
    int flagPC;
    int opcion = 0;
    int i = 0;
    int jugador[2] = {1, 2};
    string nombre[2];
    char simbolo[2] = {'X', 'O'};
    int eleccion;
    char tablero[9];

    while(1)
    {
        switch (opcion)
        {
            case 0:
                system("clear");
                cout << "-----------------\n";
                cout << "   Tic Tac Toe\n";
                cout << "-----------------\n";
                cout << "Presione:\n";
                cout << "1. Jugador vs Jugador\n";
                cout << "2. Jugador vs PC\n";
                cin >> opcion;
                system("clear");
                for (i=0; i<9; i++)
                {
                    tablero[i] = ' ';
                }
                i = 0;
                flagGanador = 0;
                flagPC = 0;
                if (opcion == 1)
                {
                    cout << "Ingrese el nombre del Jugador1: ";
                    cin >> nombre[0];
                    cout << "Ingrese el nombre del Jugador2: ";
                    cin >> nombre[1];

                }
                if (opcion == 2)
                {
                    opcion = 1;
                    flagPC = 1;
                    cout << "Ingrese el nombre del Jugador1: ";
                    cin >> nombre[0];                    
                }                
            break;
            case 1:
                system("clear");
                cout << "-----------------\n";
                cout << "   Tic Tac Toe\n";
                cout << "-----------------\n";                
                cout << "\t " << tablero [6] << " | " << tablero [7] << " | " << tablero [8] << " " << endl;
                cout << "\t " << tablero [3] << " | " << tablero [4] << " | " << tablero [5] << " " << endl;
                cout << "\t " << tablero [0] << " | " << tablero [1] << " | " << tablero [2] << " " << endl;
                cout << "\n" << nombre[i] << " elija su jugada segun el teclado numerico o 0 para ir al menu principal." << endl;
                cin >> eleccion;
                if (eleccion == 0)
                {
                    opcion = eleccion;
                    break;
                }
                if (tablero[eleccion-1] == ' ')
                {
                    tablero[eleccion-1] = simbolo[i];

                    flagGanador = check_winner(tablero);

                    if (flagGanador == 1)
                    {
                        cout << "\nGana " << nombre[i] << "!" << endl;
                        cout << "\nPresione 0 para ir al menu principal" << endl;
                        cin >> opcion;
                    }     
                    else
                    {
                        if (flagPC == 1)
                        {
                            i=1;

                            srand(time(0));
                            
                            while(tablero[eleccion-1] != ' ')
                            {
                                eleccion = (rand() % (9));
                            } 
                            
                            tablero[eleccion-1] = simbolo[i];

                            flagGanador = check_winner(tablero);

                            if (flagGanador == 1)
                            {
                                cout << "\nGana el JugadorPC!" << endl;
                                cout << "\nPresione 0 para ir al menu principal" << endl;
                                cin >> opcion;
                            }  
                                                     
                        }
                        if (i==0)
                            i++;
                        else
                            i--;                      
                    }                                                                            
                }
            break;        

            default:
                return 0;
            break;
        }
    }   
}