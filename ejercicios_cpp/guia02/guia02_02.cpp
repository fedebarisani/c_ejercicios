/*
Ejercicio 2
Desarrolle un programa en C++ que a través de una clase Punto calcule la distancia al
origen del mismo, suponiendo que es un punto en el plano. La clase deberá contener
atributos para cada coordenada y métodos de cálculo de la distancia y de acceso a los
datos. Utilice los conceptos de constructor por defecto y constructor alternativo.
*/

#include <string>
#include <iostream>
#include <cmath>

using namespace std;

/////////////////////////////////////////////////////////////////////////////////

class Punto
{
    public:
        double x = 0, y = 0;
        double dist_orig;

    //Constructor por defecto
    Punto ()
    {
        dist_orig = sqrt(pow(x,2) + pow(y,2));
        cout << "Distancia al origen: " << dist_orig << endl;
    }
    //Constructor parametrizado
    Punto (double x1, double y1)
    {
        x = x1;
        y = y1;
        dist_orig = sqrt(pow(x,2) + pow(y,2));
        cout << "Distancia al origen: " << dist_orig << endl;
    }
};

/////////////////////////////////////////////////////////////////////////////////

int main()
{
    double x, y;

    cout << "Ingrese coordenada x del punto: ";
    cin >> x;
    cout << "Ingrese coordenada y del punto: ";
    cin >> y;

    Punto(x,y);
}