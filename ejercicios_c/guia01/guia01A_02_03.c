/*
Ejercicio 2
Escribir las instrucciones scanf o printf necesarias para cada uno de los siguientes puntos:
3. Supongamos que a y b son variables enteras. Pedir al usuario que introduzca el valor de estas dos variables y mostrar después su suma. Rotular la salida adecuadamente.
*/
#include <stdio.h>

void main (void)
{
    int a, b, r;

    printf("Se realizará la suma de A + B.\nIngrese el numero A: ");
    scanf("%d", &a);
    printf("Ingrese el numero B: ");
    scanf("%d", &b);
    r = a + b;
    printf("A + B = %d + %d = %d\n", a, b, r);
}