/*
Ejercicio 2
Escribir las instrucciones scanf o printf necesarias para cada uno de los siguientes puntos:
1. Generar el mensaje: 
Por favor, introduce tu nombre:
y que el usuario introduzca en la misma línea su nombre. Asignar el nombre a una cadena de caracteres llamada nombre.
*/

#include <stdio.h>

void main (void)
{
    char nombre[50];

    printf("Por favor, introduce tu nombre: ");

    scanf("%s", nombre);

    printf("Bienvenido %s!\n", nombre);

}