/*
Ejercicio 2
Supóngase que se deposita una cantidad dada de dinero A en una cuenta de ahorros al 
principio de cada año durante n años. Si se percibe un interés anual i, entonces la 
cantidad de dinero F que se acumulara tras n años viene dada por: 
F = A * ( (1+i/100) + (1+i/100)2 + (1+i/100)3+ ... + (1+i/100)n
Escriba un programa en C de estilo conversacional que determine lo siguiente:
¿Cuánto dinero se acumulara después de 30 años si se depositan 100 dólares al comienzo 
de cada año y el tipo de interés compuesto es el 6% anual?
¿Cuánto dinero se debe depositar al comienzo de cada año para acumular 100.000 dólares 
después de 30 años, suponiendo también un tipo de interés compuesto del 6% anual?
En cada caso determinar primero la cantidad de dinero desconocida. Luego crear una tabla 
mostrando la cantidad total de dinero que se acumulara al final de cada año. Definir una 
función para la potenciación. 
*/

#include <stdio.h>

float pot (float num, int exp)
{
    int i;
    float res;
    res = num;
    for (i=1; i<exp; i++)
    {
        res = res*num; 
    }
    return res;
}

void main (void)
{
    float A; 
    float i = 6;
    double F = 0;
    double aux = 0;
    int n = 30;
    int j;

    for(j=1; j<=n; j++)
    {
        aux = aux + pot((1+i/100),j);
        //printf("%lf\n", aux);
    }
    printf("¿Cuánto dinero se acumulara después de 30 años si se\ndepositan 100 dólares al comienzo de cada año y el tipo\nde interés compuesto es el 6 porciento anual?");
    A = 100;
    F = A * aux;
    printf("\nSe acumularán %lf dólares.\n\n", F);

    printf("¿Cuánto dinero se debe depositar al comienzo de cada\naño para acumular 100.000 dólares después de 30 años, suponiendo\ntambién un tipo de interés compuesto del 6 porciento anual?");
    F = 100000;
    A = F / aux;
    printf("\nSe deben depositar %lf dólares por año.\n", A);
}