/*
Ejercicio 2
Escribir las instrucciones scanf o printf necesarias para cada uno de los siguientes puntos:
2. Supongamos que x1 y x2 son variables en coma flotante cuyos valores son 8.0 y -2.5 respectivamente. Visualizar los valores de x1 y x2, con los rótulos adecuados, por ejemplo:
x1 = 8.0	x2 = -2.5
*/

#include <stdio.h>

void main (void)
{
    float x1;
    float x2;

    printf("Ingrese el numero flotante x1: ");
    scanf("%f", &x1);
    printf("Ingrese el numero flotante x2: ");
    scanf("%f", &x2);
    printf("x1 = %.1f\tx2 = %.1f\n", x1, x2); 
}
