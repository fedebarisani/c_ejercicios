/*
Ejercicio 1
Considere el famoso problema de reordenar una lista de n números enteros. Escriba un  programa que realice la reordenación de modo que no se utilice almacenamiento innecesario. Esto es, el programa contendrá un solo arreglo unidimensional de enteros llamado x en el que se reordenarán los elementos de uno en uno. Utilice el reordenamiento del tipo burbuja.
El programa será del tipo conversacional (interactivo) donde se podrá realizar cualquiera de las siguientes operaciones:
1. Ordenamiento de menor a mayor en valor absoluto.
2. Ordenamiento de menor a mayor algebraicamente (con signo).
3. Ordenamiento de mayor a menor en valor absoluto.
4. Ordenamiento de mayor a menor algebraicamente (con signo).
Tip: Utilice los siguientes valores como arreglo de testeo: 
[4.7, -8.0, -2.3, 11.4, 12.9, 5.1, 8.8, -0.2, 6.0, -14.7]
*/

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>

typedef int (*CompareFunc)(float a, float b);


int sort_min (float a, float b)
{
    if (a <= b)
        return 1;
    else 
        return 0;
}

int sort_min_abs (float a, float b)
{
    if (abs(a) <= abs(b))
        return 1;
    else 
        return 0;
}

int sort_max (float a, float b)
{
    if (a >= b)
        return 1;
    else 
        return 0;
}

int sort_max_abs (float a, float b)
{
    if (abs(a) >= abs(b))
        return 1;
    else 
        return 0;
}

void tools_array_sort(float array[], int len, CompareFunc fun)
{
    int i, j;
    int comp;
    float aux[len];
    
    for (j=0; j<len; j++)
    {
        for (i=0; i<len-1; i++)
        {
            comp = fun(array[i],array[i+1]);
            if (comp == 0)
            {
                aux[i] = array[i];
                array[i] = array[i+1];
                array[i+1] = aux[i];
            }
        }
    }
    //Debug
    for (i=0; i<len; i++)
        printf("%.1f\t", array[i]);

    printf("\n");
}


int main (void)
{
    int i;
    int opcion;
    float float_array[] = {4.7, -8.0, -2.3, 11.4, 12.9, 5.1, 8.8, -0.2, 6.0, -14.7};
    int len = sizeof(float_array)/sizeof(float_array[0]);

    CompareFunc ptr;

    while(1)
    {
        printf("\n------------------------------------------\n");
        printf("PROGRAMA DE ORDENAMIENTO");
        printf("\n------------------------------------------\n");
        printf("1. Menor a mayor en valor absoluto\n");
        printf("2. Menor a mayor algebraicamente (con signo)\n");
        printf("3. Mayor a menor en valor absoluto\n");
        printf("4. Mayor a menor algebraicamente (con signo)\n");
        printf("\nSeleccione la opción de ordenamiento: ");
        scanf("%d", &opcion);
        printf("\n");
        for (i=0; i<len; i++)
            printf("%.1f\t", float_array[i]);
        printf("\n\n");

        switch(opcion)
        {
            case 1:
                ptr = sort_min_abs;
                tools_array_sort(float_array, len, ptr);
            break;

            case 2:
                ptr = sort_min;
                tools_array_sort(float_array, len, ptr);            
            break;

            case 3:
                ptr = sort_max_abs;
                tools_array_sort(float_array, len, ptr);                  
            break;

            case 4:
                ptr = sort_max;
                tools_array_sort(float_array, len, ptr);             
            break;

            default:
                return 0;
            break;
        }
    }  
}